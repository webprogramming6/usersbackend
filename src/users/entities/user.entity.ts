import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_login', length: 30 })
  login: string;

  @Column()
  name: string;

  @Column()
  password: string;
}
